﻿[assembly: Xamarin.Forms.ExportRenderer(
    typeof(Bible.Views.LoginFacebookPage),
    typeof(Bible.Droid.Implementations.LoginPageRenderer))]
namespace Bible.Droid.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Android.App;
    using Bible.Services;
    using Bible.Models;
    using Xamarin.Auth;
    using Xamarin.Forms.Platform.Android;
    using Views;

    public class LoginPageRenderer:PageRenderer
    {
        public LoginPageRenderer()
        {
            var activity = this.Context as Activity;
            var loginPage = Element as LoginFacebookPage;
            string providername = loginPage.ProviderName;

            if (providername == "Facebook")
            {
                var auth = new OAuth2Authenticator(
                clientId: "1848025438829033",
                scope: "email",
                authorizeUrl: new Uri("https://www.facebook.com/dialog/oauth/"),
                redirectUrl: new Uri("https://www.facebook.com/connect/login_success.html"));

                auth.Completed += async (sender, eventArgs) =>
                {
                    if (eventArgs.IsAuthenticated)
                    {
                        var accessToken = eventArgs.Account.Properties["access_token"].ToString();
                        var profile = await GetFacebookProfileAsync(accessToken);
                        await App.NavigateToProfile(profile);
                    }
                    else
                    {
                        App.HideLoginView();
                    }
                };

                activity.StartActivity(auth.GetUI(activity));
            }
            else if (providername == "Twitter")
            {
                var auth = new OAuth2Authenticator(
                clientId: "SbEsBQlURYo09lxPrvg21ANky",    // For Twitter login, for configure refer https://code.msdn.microsoft.com/Register-Identity-Provider-41955544
                clientSecret: "6BY8rf8vEQ95WdoVKkTJr8s9XqqK8djIMT1Iq2gk5DZkGHRBK6",  // For Twitter login, for configure refer https://code.msdn.microsoft.com/Register-Identity-Provider-41955544
                scope: "email",
                authorizeUrl: new Uri("https://api.twitter.com/oauth/authorize"), // These values do not need changing
                redirectUrl: new Uri("https://i2.wp.com/mch-solutions.com.mx/wp-content/uploads/2017/01/Fondo-azul.jpg"),    // Set this property to the location the user will be redirected too after successfully authenticating
                accessTokenUrl: new Uri("https://api.twitter.com/oauth/access_token")); // These values do not need changing
                                
                auth.Completed += async (sender, eventArgs) =>
                {
                    if (eventArgs.IsAuthenticated)
                    {
                        var accessToken = eventArgs.Account.Properties["access_token"].ToString();
                        var profile = await GetFacebookProfileAsync(accessToken);
                        await App.NavigateToProfile(profile);
                    }
                    else
                    {
                        App.HideLoginView();
                    }
                };

                activity.StartActivity(auth.GetUI(activity));
            }


        }

        private async Task<FacebookResponse> GetFacebookProfileAsync(string accessToken)
        {
            var requestUrl = "https://graph.facebook.com/v2.8/me/?fields=name,picture.width(999),cover," +
                "age_range,devices,email,gender,is_verified,birthday,languages,work,website,religion," +
                "location,locale,link,first_name,last_name,hometown&access_token=" + accessToken;
            var apiService = new ApiService();
            return await apiService.GetFacebook(requestUrl);
        }

        private async Task<TwitterResponse> GetTwitterProfileAsync(string accessToken)
        {
            var requestUrl = "https://graph.facebook.com/v2.8/me/?fields=name,picture.width(999),cover," +
                "age_range,devices,email,gender,is_verified,birthday,languages,work,website,religion," +
                "location,locale,link,first_name,last_name,hometown&access_token=" + accessToken;
            var apiService = new ApiService();
            return await apiService.GetTwitter(requestUrl);
        }

    }
}