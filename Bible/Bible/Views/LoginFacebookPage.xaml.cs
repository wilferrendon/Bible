﻿namespace Bible.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginFacebookPage : ContentPage
	{
        public string ProviderName { get; set; }

        public LoginFacebookPage (string _providername)
		{
			InitializeComponent ();
            ProviderName = _providername;
        }
	}
}