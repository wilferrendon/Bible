﻿


namespace Bible.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BiblesPage : ContentPage
	{
		public BiblesPage ()
		{
			InitializeComponent ();
		}
	}
}