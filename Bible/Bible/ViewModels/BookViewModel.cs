﻿
namespace Bible.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Input;
    using Bible.Helpers;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class BookViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        private Bible bible;
        private BookResponse bookResponse;
        #endregion

        #region Attributes
        private Book book;
        private bool isRefreshing;
        private ContentResponse contentResponse;
        private ObservableCollection<Verse> verses;
        private ObservableCollection<Verse> verses2;
        private string filter;
        bool isEnabled;
        bool isEnabled2;
        #endregion

        #region Properties
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        public ObservableCollection<Verse> Verses
        {
            get { return this.verses; }
            set { SetValue(ref this.verses, value); }
        }
        public string Filter
        {
            get { return this.filter; }
            set
            {
                SetValue(ref this.filter, value);
                this.Search();
            }
        }

        public string Title
        {
            get;
            set;
        }

        public bool IsEnabled
        {
            get
            {
                return this.isEnabled;
            }
            set
            {
                SetValue(ref this.isEnabled, value);
            }
        }

        public bool IsEnabled2
        {
            get
            {
                return this.isEnabled2;
            }
            set
            {
                SetValue(ref this.isEnabled2, value);
            }
        }

        #endregion

        #region Commands

        public ICommand PreviousCommand
        {
            get
            {
                return new RelayCommand(Previous);
            }
        }

        public ICommand NextCommand
        {
            get
            {
                return new RelayCommand(Next);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);

            }

        }


        #endregion


        #region Constructors
        public BookViewModel(Book book)
        {
            this.apiService = new ApiService();
            this.book = book;
            this.LoadContent("");            
        }
        #endregion

        #region Methods

        private void Previous()
        {
            var contentResult = contentResponse.Contents[0];
            this.LoadContent(contentResult.Nav.Pcc.ToString());
            MainViewModel.GetInstance().TitleBook = contentResult.Nav.PrevChapter.ToString() + ":1-" + contentResult.VersesCount.ToString();
          
        }

        private void Next()
        {            
            var contentResult = contentResponse.Contents[0];           
            this.LoadContent(contentResult.Nav.Ncc.ToString());
            MainViewModel.GetInstance().TitleBook = contentResult.Nav.NextChapter.ToString() + ":1-" + contentResult.VersesCount.ToString(); 
            
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {

                this.Verses = new ObservableCollection<Verse>(this.Verses);
            }
            else
            {
                versess(out this.verses2);

                this.Verses = new ObservableCollection<Verse>(this.verses2.Where(l => l.Text.ToLower().Contains(
                this.Filter.ToLower())));
            }
        }

        private async void LoadContent(string cap)
        {
            this.IsRefreshing = true;
            this.IsEnabled = false;
            this.IsEnabled2 = false;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                return;
            }

            var response = await this.apiService.Get<ContentResponse>(
                "http://api.biblesupersearch.com",
                "/api",
                string.Format(
                    "?bible={0}&reference={1}{2}",
                    MainViewModel.GetInstance().SelectedModule,
                    this.book.Shortname,cap));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            this.contentResponse = (ContentResponse)response.Result;
            this.IsRefreshing = false;

            versess(out this.verses2);

            this.Verses = this.verses2;

            this.IsEnabled = true;
            this.IsEnabled2 = true;

            if (this.contentResponse.Contents[0].ChapterVerse == "1")
            {
                MainViewModel.GetInstance().TitleBook = this.book.Name + " 1:1-" +
                    contentResponse.Contents[0].VersesCount.ToString();
                this.IsEnabled = false;
            }
            else if (this.contentResponse.Contents[0].Nav.Ncc != null)                
            {
                if(Convert.ToInt32(this.contentResponse.Contents[0].Nav.Ncc) == 1)
                    this.IsEnabled2 = false;
            }  
        }

        private void versess(out ObservableCollection<Verse> vers)
        {
            vers = null;
            var contentResult = contentResponse.Contents[0];

            var type = typeof(Verses);
            var properties = type.GetRuntimeFields();
            Bible bible = null;

            foreach (var property in properties)
            {
                bible = (Bible)property.GetValue(contentResult.Verses);
                if (bible != null)
                {
                    break;
                }
            }

            if (bible == null)
            {
                return;
            }

            type = typeof(Bible);
            properties = type.GetRuntimeFields();
            Dictionary<string, Verse> chapter = null;

            foreach (var property in properties)
            {
                if (property.Name.StartsWith("<Chapter"))
                {
                    chapter = (Dictionary<string, Verse>)property.GetValue(bible);

                    if (chapter != null)
                    {
                        break;
                    }
                }
            }

            var myVerses = chapter.Select(v => new Verse
            {
                Book = v.Value.Book,
                Chapter = v.Value.Chapter,
                Id = v.Value.Id,
                Italics = v.Value.Italics,
                Text = v.Value.Text,
                VerseNumber = v.Value.VerseNumber,
            });

            vers = new ObservableCollection<Verse>(myVerses);
        }


        



        #endregion
    }       
}