﻿namespace Bible.ViewModels
{
    using Models;
    using GalaSoft.MvvmLight.Command;
    using Services;
    using System.Windows.Input;
    using System.Collections.ObjectModel;
    using System;
    using Xamarin.Forms;
    using Bible.Helpers;
    using System.Collections.Generic;
    using System.Linq;

    public class QuotesViewModel:BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private Bible bible;
        private bool isRefreshing;
        private BookResponse bookResponse;
        private ObservableCollection<BookItemViewModel> books;
        private bool isRunning;
        private bool isEnabled;
        #endregion

        #region Properties
        public ObservableCollection<BookItemViewModel> Books
        {
            get { return this.books; }
            set { SetValue(ref this.books, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        #endregion

        #region Constructors
        public QuotesViewModel()
        {
            this.apiService = new ApiService();
            this.IsEnabled = true;
            this.LoadBooks();
        }
        #endregion

        #region Commands
        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }
               

        private void Search()
        {
            
        }                
        
        #endregion

        #region Methods
        private async void LoadBooks()
        {
            this.IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                return;
            }

            var response = await this.apiService.Get<BookResponse>(
                "http://api.biblesupersearch.com",
                "/api",
                string.Format("/books?language={0}", bible.LangShort));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            this.bookResponse = (BookResponse)response.Result;

            if (bible.LangShort != "en")
            {
                var response2 = await this.apiService.Get<BookResponse>(
                    "http://api.biblesupersearch.com",
                    "/api",
                    "/books?language=en");

                if (!response2.IsSuccess)
                {
                    this.IsRefreshing = false;
                    await Application.Current.MainPage.DisplayAlert(
                        Languages.Error,
                        response2.Message,
                        Languages.Accept);
                    return;
                }

                var booksResult2 = (BookResponse)response2.Result;

                for (int i = 0; i < this.bookResponse.Books.Count; i++)
                {
                    this.bookResponse.Books[i].Shortname = booksResult2.Books[i].Shortname;
                }
            }

            this.Books = new ObservableCollection<BookItemViewModel>(
                this.ToBookItemViewModel());

            /*this.Books = new ObservableCollection<BookItemViewModel>(
                this.ToBookItemViewModel().OrderBy(l => l.Name));*/
            this.IsRefreshing = false;
        }
        
        private IEnumerable<BookItemViewModel> ToBookItemViewModel()
        {
            return this.bookResponse.Books.Select(b => new BookItemViewModel
            {
                Id = b.Id,
                Name = b.Name,
                Shortname = b.Shortname,
            });
        }
        #endregion
    }
}
