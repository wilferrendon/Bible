﻿
namespace Bible.ViewModels
{
    using Views;
    using Models;
    using System.Collections.ObjectModel;
    using System;
    using Helpers;
    using Domain;

    public class MainViewModel:BaseViewModel
    {
        #region Attributes

        private UserLocal user;
        private string titleBook;
        private string titleBible;

        #endregion

        #region ViewModels
        public BiblesViewModel Bibles
        {
            get;
            set;
        }

        public BibleViewModel Bible
        {
            get;
            set;
        }
        
        public BookViewModel Book
        {
            get;
            set;
        }

        public LoginViewModel Login
        {
            get;
            set;
        }

        public RegisterViewModel Register
        {
            get;
            set;
        }

        public MyProfileViewModel MyProfile
        {
            get;
            set;
        }

        public QuotesViewModel Quotes
        {
            get;
            set;
        }

        public ChangePasswordViewModel ChangePassword
        {
            get;
            set;
        }

        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }

        public UserLocal User
        {
            get { return this.user; }
            set { SetValue(ref this.user, value); }
        }

        public TokenResponse Token
        {
            get;
            set;
        }

        public string TitleBook
        {
            get { return this.titleBook; }
            set { SetValue(ref this.titleBook, value); }
        }

        public string TitleBible
        {
            get { return this.titleBible; }
            set { SetValue(ref this.titleBible, value); }
        }

        #endregion

        #region Properties
        public string SelectedModule
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;

            this.Login = new LoginViewModel();
            this.LoadMenu();
        }

       
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Methods

        private void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();
            this.Menus.Add(new MenuItemViewModel
            {
                Icon ="ic_settings",
                PageName = "MyProfilePage",
                Title =Languages.MyProfile,

            });

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_format_quote",
                PageName = "QuotesPage",
                Title = Languages.Quotes,

            });

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_vpn_key",
                PageName = "KeyWordPage",
                Title = Languages.Keyword,

            });

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_exit_to_app",
                PageName = "LoginPage",
                Title = Languages.LogOut,

            });

        }
        #endregion

    }
}

