﻿
namespace Bible.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Services;
    using System.Windows.Input;
    using Xamarin.Forms;
    using ViewModels;
    using Views;
    using Helpers;
    using Configuration;

    public class LoginViewModel:BaseViewModel
    {

        #region Services

        private ApiService apiService;
        private DataService dataService;

        #endregion


        #region Atributes

        string email;
        string password;
        bool isRunning;
        bool isRemember;
        bool isEnabled;
        #endregion




        #region Properties

        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                SetValue(ref this.email, value);
            }
        }

        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                SetValue(ref this.password, value);
            }
        }

        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }
            set
            {
                SetValue(ref this.isRunning, value);
            }
        }

        public bool IsRemember
        {
            get
            {
                return this.isRemember;
            }
            set
            {
                SetValue(ref this.isRemember, value);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return this.isEnabled;
            }
            set
            {
                SetValue(ref this.isEnabled, value);
            }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        public ICommand LoginFacebookComand
        {
            get
            {
                return new RelayCommand(LoginFacebook);
            }
        }

        public ICommand LoginTwitterComand
        {
            get
            {
                return new RelayCommand(LoginTwitter);
            }
        }

        public ICommand LoginYahooComand
        {
            get
            {
                return new RelayCommand(LoginYahoo);
            }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new RelayCommand(Register);
            }
        }



        #endregion

        #region Methods 

        private async void LoginFacebook()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new LoginFacebookPage("Facebook"));
        }

        private async void LoginTwitter()
        {
            if (OAuthConfig.User == null)
            {
                await Application.Current.MainPage.Navigation.PushAsync(new LoginFacebookPage("Twitter"));
            }            
        }

        private async void LoginYahoo()
        {
            if (OAuthConfig.User == null)
            {
                await Application.Current.MainPage.Navigation.PushAsync(new LoginFacebookPage("Yahoo"));
            }
        }


        private async void Login()
        {

            if (string.IsNullOrEmpty(Email))
            {
                await Application.Current.MainPage.DisplayAlert(Languages.Error,
                     Languages.EmailValidation, Languages.Error);
            }
            else if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert(Languages.Error,
                     Languages.Password, Languages.Error);
            }

            var connection = await this.apiService.CheckConnection();

            if(!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                return;
            }
                        
            var token = await this.apiService.GetToken(
                "http://landsapi1.azurewebsites.net", 
                this.Email, 
                this.Password);

            if(token == null)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.SomethingWrong,
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    token.ErrorDescription,
                    Languages.Accept);
                this.Password = string.Empty;
                return;

            }

            //GET USER
            
            var user = await this.apiService.GetUserByEmail(
               "http://landsapi1.azurewebsites.net",
               "/api",
               "/Users/GetUserByEmail",
               token.TokenType,
               token.AccessToken,
               this.Email);

            var userLocal = Converter.ToUserLocal(user);
            userLocal.Password = this.Password;

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;
            mainViewModel.User = userLocal;

            if(this.IsRemember)
            {
                Settings.IsRemembered = "true";
            }
            else
            {
                Settings.IsRemembered = "false";
            }

            this.dataService.DeleteAllAndInsert(userLocal);
            this.dataService.DeleteAllAndInsert(token);
                
            mainViewModel.Bibles = new BiblesViewModel();
            //await Application.Current.MainPage.Navigation.PushAsync(new BiblesPage());
            Application.Current.MainPage = new MasterPage();

            this.IsRunning = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

            

        }

        private async void Register()
        {
            MainViewModel.GetInstance().Register = new RegisterViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }

        #endregion

        #region Constructors

        public LoginViewModel()
        {
            this.apiService = new ApiService();
            this.dataService = new DataService();

            this.IsRemember = true;
            this.IsEnabled = true;
            this.IsRunning = false;

        }

        #endregion

    }
}
