﻿namespace Bible.Configuration
{
    using System;
    using Xamarin.Forms;
    using Views;
    using ViewModels;
    using Models;
    using Services;
    using Helpers;

    public class OAuthConfig
    {
        public static BiblesPage  _HomePage;
        //public static NavigationPage _NavigationPage;
        public static UserDetails User;

        public static Action SuccessfulLoginAction
        {
            get
            {
                return new Action(() =>
                {
                    var dataService = new DataService();

                    var token = OAuthConfig.User.Token;

                    if (token == null)
                    {
                        Application.Current.MainPage = new NavigationPage(new LoginPage());
                        return;
                    }


                    var user = OAuthConfig.User;

                    UserLocal userLocal = null;
                    if (user != null)
                    {
                        userLocal = Converter.ToUserLocal2(user);
                        dataService.DeleteAllAndInsert(userLocal);
                        dataService.DeleteAllAndInsert(token);
                    }

                    var mainViewModel = MainViewModel.GetInstance();
                    mainViewModel.Token = Converter.ToToken(token);
                    mainViewModel.User = userLocal;
                    mainViewModel.Bibles = new BiblesViewModel();
                    Application.Current.MainPage = new MasterPage();
                    Settings.IsRemembered = "true";

                   // var mainViewModel = MainViewModel.GetInstance();
                   // mainViewModel.Bibles = new BiblesViewModel();
                    //await Application.Current.MainPage.Navigation.PushAsync(new BiblesPage());
                   // Application.Current.MainPage = new MasterPage();

                   // _NavigationPage.Navigation.PushModalAsync(_HomePage);
                });
            }
        }
    }
}
