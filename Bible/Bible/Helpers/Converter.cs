﻿namespace Bible.Helpers
{
    using System;
    using Domain;
    using Models;
    using Configuration;

    public static class Converter
    {
        public static UserLocal ToUserLocal(User user)
        {
            return new UserLocal
            {
                Email = user.Email,
                FirstName = user.FirstName,
                ImagePath = user.ImagePath,
                LastName = user.LastName,
                Telephone = user.Telephone,
                UserId = user.UserId,
                UserTypeId = user.UserTypeId,
            };
        }

        public static UserLocal ToUserLocal2(UserDetails user)
        {
            return new UserLocal
            {
                Email = user.ScreenName,
                FirstName = user.ScreenName,
                UserId = Convert.ToInt32(user.TwitterId),
                UserTypeId = 2,
            };
        }

        public static TokenResponse ToToken(string token)
        {
            return new TokenResponse
            {
                AccessToken = token,
            };
        }


        public static User ToUserDomain(UserLocal user, byte[] imageArray)
        {
            return new User
            {
                Email = user.Email,
                FirstName = user.FirstName,
                ImagePath = user.ImagePath,
                LastName = user.LastName,
                Telephone = user.Telephone,
                UserId = user.UserId,
                UserTypeId = user.UserTypeId.Value,
                ImageArray = imageArray,
            };
        }
    }
}
